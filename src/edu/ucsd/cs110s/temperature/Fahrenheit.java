/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author zec003
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return ""+this.getValue()+"F";
	}
	@Override
	public Temperature toCelsius() {
		float value = getValue();
		value = (float) ((double)5/(double)9*(value-32));
		return new Celsius(value);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
}
