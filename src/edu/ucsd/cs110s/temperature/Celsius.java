package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return ""+this.getValue()+"C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float value = getValue();
		System.out.println(value + 32);
		value = (float)(((double)9/(double)5)*value + 32);
		System.out.println(value);
		return new Fahrenheit(value);
	}
}
